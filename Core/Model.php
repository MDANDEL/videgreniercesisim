<?php

namespace Core;

use App\ConfigPreProd;
use App\ConfigProd;
use PDO;
use App\Config;

/**
 * Base model
 *
 * PHP version 7.0
 */
abstract class Model
{

    /**
     * Get the PDO database connection
     *
     * @return mixed
     */
    protected static function getDB()
    {
        static $db = null;

        if ($db === null) {
            $env = parse_ini_file('../.env');

            if ($env[ 'ENV_PROD' ] === 'PROD') {
                $dsn = 'mysql:host=' . ConfigProd::DB_HOST . ';port=' . ConfigProd::DB_PORT . ';dbname=' . ConfigProd::DB_NAME . ';charset=utf8';
                $db = new PDO($dsn, ConfigProd::DB_USER, ConfigProd::DB_PASSWORD);
            } elseif ($env[ 'ENV_PROD' ] === 'PREPROD') {
                $dsn = 'mysql:host=' . ConfigPreProd::DB_HOST . ';port=' . ConfigPreProd::DB_PORT . ';dbname=' . ConfigPreProd::DB_NAME . ';charset=utf8';
                $db = new PDO($dsn, ConfigPreProd::DB_USER, ConfigPreProd::DB_PASSWORD);
            } elseif ($env[ 'ENV_PROD' ] === 'DEV') {
                $dsn = 'mysql:host=' . Config::DB_HOST . ';port=' . Config::DB_PORT . ';dbname=' . Config::DB_NAME . ';charset=utf8';
                $db = new PDO($dsn, Config::DB_USER, Config::DB_PASSWORD);
            }

            // Throw an Exception when an error occurs
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        return $db;
    }
}
