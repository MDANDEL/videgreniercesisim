<?php

namespace App;

/**
 * Application configuration
 *
 * PHP version 7.0
 */
class Config
{
    /**
     * Database user
     * @var string
     */
    const DB_USER = 'webapplication';

    /**
     * Database Host
     * @var string
     */
    const DB_HOST = 'mysql-dev';

    /**
     * Database Port
     * @var string
     */
    const DB_PORT = 3306;

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = '653rag9T';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'videgrenierenligne_dev';

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = true;
}