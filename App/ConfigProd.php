<?php

namespace App;

class ConfigProd
{
    /**
     * Database user
     * @var string
     */
    const DB_USER = 'webapplication';

    /**
     * Database Host
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * Database Port
     * @var string
     */
    const DB_PORT = 4500;

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = '653rag9T';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'videgrenierenligne_prod';

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = true;
}