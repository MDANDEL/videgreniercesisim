<?php

namespace App\Utility;

class Flash
{
    public static function danger($message)
    {
        $_SESSION['flash_message'] = [
            'type' => 'danger',
            'message' => $message
        ];
    }

    // Add more methods for different types of flash messages, such as success, warning, etc.
}
