<?php

namespace App\Controllers;

use \Core\View;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


/**
 * Home controller
 */
class Home extends \Core\Controller
{

    /**
     * Affiche la page d'accueil
     *
     * @return void
     * @throws \Exception
     */
    public function indexAction()
    {

        View::renderTemplate('Home/index.html', []);
    }

    public function dashboardAction()
    {

        View::renderTemplate('Home/dashboard.html', []);
    }

    public function contactAction()
    {



        View::renderTemplate('Home/contact.html', []);
    }


    public function sendAction()
    {


        $email = $_POST['email'];
        $message = $_POST['message'];

        try {


            $mail = new PHPMailer(true);
            $mail->isSMTP();
            $mail->Host = 'smtp.mailtrap.io';
            $mail->SMTPAuth = true;
            $mail->Username = '6af039d0b778fd';
            $mail->Password = '7d7de1147afa65';
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = 2525;


            $mail->setFrom($email, 'From Name');
            $mail->addAddress('to@example.com', 'To Name');
            $mail->Subject = 'Test Email';
            $mail->Body = "$message";


            if ($mail->send()) {

                View::renderTemplate('Home/contact.html', ['message' => 'Message envoyé avec succès']);
            } else {


                View::renderTemplate('Home/contact.html', ['message' => "une erreur est servenu"]);
            }
        } catch (Exception $e) {

            View::renderTemplate('Home/contact.html', ['message' => "une erreur est servenu : $e"]);
        }
    }
}
